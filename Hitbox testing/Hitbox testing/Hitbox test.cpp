#include"Hitbox.h";
#include<time.h>;

void main()
{
	float StartTime = clock();
	float EndTime; 
	Shape Shape1, Shape2;

	Shape1.setMeasurements(-20, 0, 5, 10);
	Shape2.setMeasurements(20, 0, 5, 10);

	while(true)
	{
		EndTime = clock();
		float currentTime = (StartTime - EndTime) / CLOCKS_PER_SEC;
		Shape1.moveX(3*+-currentTime);
		StartTime = EndTime;
		std::cout << "Box 1 position : " << Shape1.location.x << std::endl;
		
		if (Shape1.colliding()
		{
			std::cout << "Your box has collided!";
			
		}
	}
}