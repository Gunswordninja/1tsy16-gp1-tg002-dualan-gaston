#include <stdio.h>
#include <iostream>


struct Vector2
{
	float x;
	float y;
};

struct Hitbox
{
	float x1,x2,y1,y2;
	
	void sethitBox(Vector2 location, float length, float width)
	{
		this -> x1 = location.x - length/2;
		this -> x2 = location.x + length/2;
		this -> y1 = location.y - length/2;
		this -> y2 = location.y + length/2;
	}
};

class Shape
{
private:

	
	float height, length;
	Hitbox *outerHitbox;

public:

	Hitbox objectHitbox;
	Hitbox *getHitbox() {return outerHitbox;}
	Vector2 location;

	Shape (float setX, float setY, float newHeight, float newLength)
	{
		this ->location.x = setX;
		this ->location.y = setY;
		this ->height = newHeight;
		this ->length = newLength;
		this ->objectHitbox.sethitBox(location,length,height);
	}

	bool colliding(Hitbox *f)
	{
		if (this->objectHitbox.x1 <= f->x2 && this->objectHitbox.x2 > f->x1) //&& this->objectHitbox.x2 >= objectHitbox.x1)//||this->objectHitbox.y1 <= objectHitbox.y2||this->objectHitbox.y2 >= objectHitbox.y1)
		{return true;}

		else {return false;}
	}

	void moveX(float move)
	{
		this -> location.x += move;
		this -> objectHitbox.x1 += move;
		this -> objectHitbox.x2 += move;
	}

	void moveY(float move)
	{
		this -> location.y += move;
		this -> objectHitbox.y1 += move;
		this -> objectHitbox.y2 += move;
	}

};