﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ResistanceBase
{
    class Program
    {
        static Random random = new Random();
        static int GetNumSpies(int totalPlayers)
        {
            // 5-6 players = 2 spies
            // 7-8 players = 3 spies
            // 9-10 players = 4 spies
            int remainder;
            int SpiesAlgorhythm = Math.DivRem(totalPlayers, 2, out remainder);
            int SpiesNum = SpiesAlgorhythm;
            if (remainder == 0) { SpiesNum--; }
            return SpiesNum;
        }

        static int EvaluateRound(string[] players)
        {
            // Return 0 if resistance wins
            // Return 1 if spies win

            Random Randomizer = new Random();
            bool[] PreventRepeat = new bool[players.Length];
            int MissionPlayer = Randomizer.Next(0, players.Length);
            int Result = 0;
            for (int i = 0; i < players.Length; i++)
            {
                string choice;
                while (PreventRepeat[MissionPlayer])
                { MissionPlayer = Randomizer.Next(0, players.Length);}
                    if (!PreventRepeat[MissionPlayer]) { PreventRepeat[MissionPlayer] = true; }
                    Console.WriteLine(players[MissionPlayer] + " S/F");
                    while (true)
                    {
                        choice = Console.ReadLine();
                        if (choice == "S") { break; }
                        if (choice == "F") { Result++; break; }
                    }                              
            }
            if (Result == 0) { return 0; }
            else { return 1; }
        }


        static void PrintPlayers(string[] players)
        {
            for (int i = 0; i < players.Length; i++)
            {
                Console.WriteLine("Player " + (i + 1) + ": " + players[i]);
            }
        }

        static void AssignRoles(string[] players)
        {
            Random Randomizer = new Random();
            // Must be dependent on the number of spies
            // Must always reach max # of spies. e.g. if there are 5 players there are always 2 spies
            // Must not exceed the # of spies for that set 
            // e.g if there are 7 players, there are always 3 spies, nothing less nothing more
            int spiesNum = GetNumSpies(players.Length);
            while (true)
            {
                int SpyCount = 0;
                for (int x = 0; x < players.Length; x++)
                {
                    int RoleChoice = Randomizer.Next(0, 2);
                    if (RoleChoice == 0) { players[x] = "Player " + (x + 1) + ": Resistance"; }
                    else { players[x] = "Player " + (x + 1) + ": Spies"; SpyCount++; }
                }
                if (SpyCount == spiesNum) { break; }
            }

            PrintPlayers(players);
            Console.WriteLine("There are " + spiesNum + " spies");
        }

        static int GetPlayersForMission(int totalPlayers)
        {
            // 5-6 players = 3 per mission
            // 7-8 players = 4 per mission
            // 9-10 players = 5 per mission
            int remainder;
            int MissionAlgorhythm = Math.DivRem(totalPlayers, 2, out remainder);
            if (remainder != 0) { MissionAlgorhythm++; }
            return MissionAlgorhythm;
        }

        static bool ValidateIndex(int[] missionPlayersIndex, int randomIndex)
        {
            for (int j = 0; j < missionPlayersIndex.Length; j++)
            {
                int presentIndex = missionPlayersIndex[j];
                if (presentIndex == randomIndex)
                {
                    return true;
                }
            }
            return false;
        }

        static void Main(string[] args)
        {
            Console.WriteLine("Welcome to the Resistance!");

            int playersNum = 0;
            /////////////////////
            // QUIZ ITEM: Loop until the player enters a valid number
            /////////////////////
            while (true)
            {
                Console.Write("Enter number of players (5-10)");
                playersNum = Convert.ToInt32(Console.ReadLine());
                if (playersNum <= 10 && playersNum >= 5) { break; }
            }
            // END LOOP

            // Make an array of players based on the number the player entered
            string[] players = new string[playersNum];
            // Assign roles to the players
            AssignRoles(players);
            Console.Read();

            int resistanceScore = 0;
            int spiesScore = 0;
            int roundNum = 1;
            // Begin the missions
            while (resistanceScore < 3 && spiesScore < 3)
            {
                Console.Clear();
                Console.WriteLine("Mission " + roundNum.ToString());
                Console.WriteLine("Resistance: " + resistanceScore.ToString());
                Console.WriteLine("Spies: " + spiesScore.ToString());

                // Select random players based on the players array
                int missionPlayerCount = GetPlayersForMission(playersNum);
                List<int> missionPlayersIndex = new List<int>();
                string[] missionPlayers = new string[missionPlayerCount];
                for (int i = 0; i < missionPlayerCount; i++)
                {
                    int randomIndex = 0;
                    bool alreadyTaken = true;
                    while (alreadyTaken)
                    {
                        randomIndex = random.Next(0, players.Length);
                        alreadyTaken = ValidateIndex(missionPlayersIndex.ToArray(), randomIndex);
                    }
                    missionPlayersIndex.Add(randomIndex);
                    missionPlayers[i] = players[randomIndex];
                }
                // END select random players

                // Play the mission
                int result = EvaluateRound(missionPlayers);
                // Add the result score
                if (result == 0)
                {
                    resistanceScore++;
                }
                else
                {
                    spiesScore++;
                }
                roundNum++;
            }
            // END missions

            if (resistanceScore >= 3)
            {
                Console.WriteLine("Resistance Wins!");
            }
            else if (spiesScore >= 3)
            {
                Console.WriteLine("Spies Wins!");
            }
            Console.ReadKey();

        }
    }
}
