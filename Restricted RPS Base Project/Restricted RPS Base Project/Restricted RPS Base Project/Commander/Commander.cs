﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestrictedRPS
{
    public abstract class Commander
    {
        public string Name { get; set; }
        public int Points { get; protected set; }

        private List<Card> cards = new List<Card>();
        public IList<Card> Cards { get { return cards; } }
        public bool EmptyHand { get { return cards.Count == 0; } }

        /// <summary>
        /// Can only discard if commander has at least 2 cards.
        /// </summary>
        public bool CanDiscard
        {
            get
            {
                return cards.Count < 2 == false;
            }
        }

        /// <summary>
        /// Draw a random card and add it to your hand.
        /// </summary>
        /// <returns></returns>
        public Card Draw()
        {
            int random = RandomHelper.Range(3);

            switch (random)
            {
                case 0: return new Assassin();

                case 1: return new Warrior();

                case 2: return new Mage();

                default: return null;
            }
        }

        /// <summary>
        /// Discards two random cards in exchange of one random card. Cannot discard if player has only one card (throws an exception)
        /// </summary>
        /// <returns>Received card after discarding.</returns>
        public Card Discard()
        {
            if (!CanDiscard) throw new Exception("Cannot discard now."); // DON'T DELETE THIS LINE

            else
            {
                for (int i = 0;  i < 2; i++)
                {
                    Cards.RemoveAt(RandomHelper.Range(Cards.Count));
                }

                return Draw();
            }
        }

        /// <summary>
        /// Display this commander's cards. NOTE: Only call this for the player's turn.
        /// </summary>
        public void DisplayCards()
        {
            foreach (Card cards in cards)
            {
                cards.setType();
                Console.WriteLine(cards.Type);
            }
        }

        //removes selected cards at selected 

        public void RemoveCard(Card selectedCard)
        {
            foreach (Card card in Cards)
            {
                if (card.GetType() == selectedCard.GetType())
                {
                    Cards.Remove(card);
                    break;
                }
            }
        }
        /// <summary>
        /// Called whenever one side's hand is empty. All cards will be discarded. Each discarded card will reduce the player's point by 1.
        /// </summary>
        public void OnGameEnding()
        {
            if (Cards.Count == 0)
            {

            }
        }

        /// <summary>
        /// Commander's action upon his turn. Can either "Play" or "Discard"
        /// </summary>
        /// <param name="opponent"></param>
        public abstract void EvaluateTurn(Commander opponent);

        /// <summary>
        /// Choose a card to play. Card must be discarded after playing.
        /// </summary>
        /// <returns>Card to play.</returns>
        public abstract Card PlayCard();

        /// <summary>
        /// Each commander plays a card. Points are evaluated here.
        /// Free code :)
        /// </summary>
        /// <param name="opponent"></param>
        public void Fight(Commander opponent)
        {
            Card myCard = PlayCard();
            Card opponentCard = opponent.PlayCard();
            myCard.setType();
            opponentCard.setType();

            Console.WriteLine("You play a " + myCard.Type);
            Console.WriteLine("The computer plays a " + opponentCard.Type);

            FightResult result = myCard.Fight(opponentCard);
            if (result == FightResult.Win)
            {
                Console.WriteLine("The " + myCard.Type + " wins!");
                Console.WriteLine("The " + opponentCard.Type + " loses!");
                Points += 2;
            }
            if (result == FightResult.Draw)
            {
                Console.WriteLine("Its a draw!");
            }
            if (result == FightResult.Lose)
            {
                Console.WriteLine("The " + myCard.Type + " loses!");
                Console.WriteLine("The " + opponentCard.Type + " wins!");
            }
        }
    }
}
