﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestrictedRPS
{
    public class AI : Commander
    {
        /// <summary>
        /// 30% chance to discard, 70% chance to play a card.
        /// </summary>
        public override void EvaluateTurn(Commander opponent)
        {
            Console.WriteLine("It is the AI's turn");
            Console.WriteLine();
            opponent.DisplayCards();
            Console.WriteLine();
            int randomAction  = RandomHelper.Range(100);

            if (randomAction > 30)
            {
                Console.WriteLine("AI has challenged the player to a duel!");
                Fight(opponent);
            }
            else
            {
                Console.WriteLine("AI has discarded 2 cards and drew a new one!");
                Cards.Add(Discard());
            }
        }

        public override Card PlayCard()
        {
            int choice = RandomHelper.Range(3);

            switch (choice)
            {
                case 0: return new Warrior();

                case 1: return new Assassin();

                case 2: return new Mage();

                default: return null;
            }
        }
    }
}
