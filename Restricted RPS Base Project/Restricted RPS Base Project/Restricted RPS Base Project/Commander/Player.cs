﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestrictedRPS
{
    public class Player : Commander
    {
        /// <summary>
        /// Get player's input. Player can only either Play or Discard.
        /// </summary>
        public override void EvaluateTurn(Commander opponent)
        {
            Console.WriteLine("It is your turn");
            Console.WriteLine("1 - Play a card");
            Console.WriteLine("2 - Discard 2 cards and play a random card");
            DisplayCards();
            int choice = Convert.ToInt32(Console.ReadLine());
            
            switch(choice)
            {
                case 1: Fight(opponent);break;

                case 2:
                    Console.WriteLine("You discarded two cards and drew a new one!");
                    Card draw = Discard();
                    draw.setType();
                    Console.WriteLine("You drew a " + draw.Type + ".");
                    Cards.Add(draw);
                    break;

                default: break;
            }

        }

        /// <summary>
        /// Show a list of cards to choose from. If there are 2 cards of the same type (eg. Warrior), only show one of each type.
        /// </summary>
        /// <returns></returns>
        public override Card PlayCard()
        {
            bool warrior =false;
            bool assassin = false;
            bool mage = false;

            foreach (Card cards in Cards)
            {
                if (cards is Warrior) {  warrior = true; }
                if (cards is Assassin) { assassin = true; }
                if (cards is Mage) { mage = true; }
            }

            while (true)
            {

                Console.WriteLine();
                Console.WriteLine("Available Cards");

                if (warrior)
                { Console.WriteLine("1.) Warrior"); }
                if (!warrior)
                { Console.WriteLine("Warrior not available"); }
                if (assassin)
                { Console.WriteLine("2.) Assassin"); }
                if (!assassin)
                { Console.WriteLine("Assassin not available"); }
                if (mage)
                { Console.WriteLine("3.) Mage"); }
                if (!mage)
                { Console.WriteLine("Mage not available"); }

                int choice = Convert.ToInt32(Console.ReadLine());

                switch (choice)
                {
                    case 1:
                        if (warrior)
                        {
                            RemoveCard(new Warrior());
                            return new Warrior();
                        }
                        if (!warrior)
                        {
                            Console.WriteLine("Warrior is not available!");
                            Console.ReadKey();
                            Console.Clear();
                        }
                        break;
                    case 2:
                        if (assassin)
                        {
                            RemoveCard(new Assassin());
                            return new Assassin();
                        }
                        if (!assassin)
                        {
                            Console.WriteLine("Assassin is not available!");
                            Console.ReadKey();
                            Console.Clear();
                        }
                        break;
                    case 3:
                        if (mage)
                        {
                            RemoveCard(new Mage());
                            return new Mage();
                        }
                        if (!mage)
                        {
                            Console.WriteLine("Mage is not available!");
                            Console.ReadKey();
                            Console.Clear();
                        }
                        break;
                    default: return null;
                }
            }
        }
    }
}
