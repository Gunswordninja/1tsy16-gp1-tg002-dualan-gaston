﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestrictedRPS
{
    public class Assassin : Card
    {
        public override FightResult Fight(Card opponentCard)
        {
            switch 
                (opponentCard.Type)
            {
                case "Warrior": return FightResult.Lose; 
                case "Mage": return FightResult.Win;
                case "Assassin": return FightResult.Draw;
                default: return FightResult.Draw;
            }
           
        }

        public override void setType()
        {
            Type = "Assassin";
        }
    }
}
