﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestrictedRPS
{
    public class Warrior : Card
    {
        public override FightResult Fight(Card opponentCard)
        {

            switch
               (opponentCard.Type)
            {
                case "Warrior": return FightResult.Draw;
                case "Mage": return FightResult.Lose;
                case "Assassin": return FightResult.Win;
                default: return FightResult.Draw;
            }
        }

        public override void setType()
        {
            Type = "Warrior";
        }
    }
}
