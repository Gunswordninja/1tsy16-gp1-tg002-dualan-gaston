﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestrictedRPS
{
    public class Mage : Card
    {
        public override FightResult Fight(Card opponentCard)
        {
            switch
               (opponentCard.Type)
            {
                case "Warrior": return FightResult.Win;
                case "Mage": return FightResult.Draw;
                case "Assassin": return FightResult.Lose;
                default: return FightResult.Draw;
            }
        }

        public override void setType()
        {
            Type = "Mage";
        }
    }
}
