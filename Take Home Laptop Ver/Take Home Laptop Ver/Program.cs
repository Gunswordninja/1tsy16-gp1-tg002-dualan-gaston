﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Take_Home_Laptop_Ver
{
    class Program
    {
        class Monster
        {
            private string names;
            private int level;
            private int HP;
            private int MP;
            private int Attack;
            private int Defense;

            public Monster()
            {
                names = "insert name";
                level = 1;
                HP = 1;
                MP = 1;
                Attack = 1;
                Defense = 1;
            }

            public string SetName
            {
                get { return SetName; }
                set { names = value; }
            }

            public int SetLevel
            {
                get { return SetLevel; }
                set { level = value; }
            }

            public int SetHP
            {
                get { return SetHP; }
                set { HP = value; }
            }

            public int SetMP
            {
                get { return SetMP; }
                set { MP = value; }
            }

            public int SetAttack
            {
                get { return SetAttack; }
                set { Attack = value; }
            }

            public int SetDefense
            {
                get { return SetDefense; }
                set { Defense = value; }
            }

            public string GetStats(int Stat)
            {
               switch (Stat)
               {
            case 0 :
                return "Name: " +names;
            case 1 :
                return "Level: " + level.ToString();
            case 2 :
                return "HP: " + HP.ToString();
            case 3 :
                return "MP: " + MP.ToString();
            case 4 :
                return "Attack: " + Attack.ToString();
            case 5 :
                return "Defense: " + Defense.ToString();
            default: 
                return "";
                   }
            }

        }

        static void DataInput(int MonNum, List<Monster> MonstersList)
        {
            
            string[] pleaseEnter = { "Please Enter : Monster ", "Name", "Level", "HP", "MP", "Attack", "Defense" };

            for (int i = 0; i < MonNum; i++)
            {
                Monster Monsters = new Monster();

                for (int x = 0; x < pleaseEnter.Length - 1; x++)
                {
                    Console.WriteLine(pleaseEnter[0] + pleaseEnter[x + 1]);
                    switch (x)
                    {
                        case 0:
                            Monsters.SetName = Console.ReadLine();
                            break;
                        case 1:
                            Monsters.SetLevel = Convert.ToInt32(Console.ReadLine());
                            break;
                        case 2:
                            Monsters.SetHP = Convert.ToInt32(Console.ReadLine());
                            break;
                        case 3:
                            Monsters.SetMP = Convert.ToInt32(Console.ReadLine());
                            break;
                        case 4:
                            Monsters.SetAttack = Convert.ToInt32(Console.ReadLine());
                            break;
                        case 5:
                            Monsters.SetDefense = Convert.ToInt32(Console.ReadLine());
                            break;
                        default: continue;
                    }
                }
                MonstersList.Add(Monsters);
                Console.Clear();
            }
        }

        static void Main(string[] args)
        {
            List<Monster> MonstersList = new List<Monster>();
            Console.WriteLine("Input number of Monsters you want");
            int MonNum = Convert.ToInt32(Console.ReadLine());
            //Monster[] MonArray = new Monster[MonNum];
            string[] pleaseEnter = { "Please Enter : Monster ", "Name", "Level", "HP", "MP", "Attack", "Defense" };

            DataInput(MonNum,MonstersList);
            Console.Clear();
        Printer:
            int MonsterListNumber = 1;
            foreach (Monster Monsters in MonstersList)
            {
                Console.WriteLine("==========================");
                Console.WriteLine("Monster " + (MonsterListNumber));
                Console.WriteLine("==========================");
                for (int i = 0; i < 6; i++)
                {
                   
                    Console.WriteLine(Monsters.GetStats(i) + "\n"); 
                  }
                MonsterListNumber++;
            }
            Console.ReadKey();

            while (true)
            {
                Console.WriteLine("\nWhat would you like to do next?\n 1. Add a Monster\n 2. Remove a Monster\n 3. Exit");
                int Choice = Convert.ToInt32(Console.ReadLine());
                switch(Choice)
                {
                    case 1:
                        {
                            Console.Clear();
                            DataInput(1, MonstersList);
                            //MonNum++;
                            goto Printer;                           
                        }
                    case 2:
                        {
                            Console.WriteLine("Select Monster Number to remove");
                            int Remove = Convert.ToInt32(Console.ReadLine())-1;
                            if (Remove < MonNum)
                            {
                                Console.WriteLine("You have removed " + MonstersList[Remove].GetStats(0));
                                MonstersList.RemoveAt(Remove);
                            }
                            break;
                        }
                    case 3:
                        { Environment.Exit(0); break; }

                    default: break;
                }

            }
        }
    }
}
