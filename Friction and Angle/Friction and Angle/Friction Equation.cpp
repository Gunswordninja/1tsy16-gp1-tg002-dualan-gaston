#include <math.h>
#include <time.h>
#include <conio.h>
#include <iostream>
#define KEY_UP 72
#define KEY_DOWN 80

float AngleEquation(float Angle, float pi)
{
	return cos(Angle * pi/180);
}

void main()
{
	using::std::cout;
	using::std::endl;

	float pi = 3.14159;
	float Angle = 0;
		
	float Gravity = 9.8;
	float ObjectMass = 20;
	float WeightForce = Gravity * ObjectMass;
	float TotalTime = 0;

	float NormalForce = WeightForce * AngleEquation(Angle,pi);
									//static //kinetic
	float coefficientOfFriction[2] = { 1 , .09 };
	float FrictionalForce = NormalForce * coefficientOfFriction[0];

	float NetForce = (-WeightForce + NormalForce) -  FrictionalForce;

	float StartTime = clock();

	float InitialVelocityX = 0;
	float InitialVelocityY = 0;
	float InitialDistanceX = 0;
	float InitialDistanceY = 0;

	float FinalVelocityX = 0;
	float FinalDistanceX = 0;
	float Xacceleration;
	float Yacceleration;
	float FinalVelocityY=0;
	float FinalDistanceY=0;

	while (true)
	{
		WeightForce = Gravity * ObjectMass;
		NormalForce = WeightForce * AngleEquation(Angle,pi);
		FrictionalForce =  NormalForce * coefficientOfFriction[0];
		NetForce = (WeightForce - NormalForce) -  FrictionalForce ;

		float EndTime = clock();
		float ProcessTime = EndTime -StartTime;
		float ClockTimeintoSeconds = ProcessTime/CLOCKS_PER_SEC;
		TotalTime += ClockTimeintoSeconds;

		//Acceleration F = M * A
		if (NetForce > FrictionalForce)
		 {
		 Xacceleration = NetForce/ObjectMass;
		 FinalVelocityX = (Xacceleration * ClockTimeintoSeconds) + InitialVelocityX;
		 FinalDistanceX = (FinalVelocityX * ClockTimeintoSeconds) + InitialDistanceX;
		 Yacceleration = (WeightForce-NormalForce)/ObjectMass;
		 FinalVelocityY = (Yacceleration * ClockTimeintoSeconds) + InitialVelocityY;
		 FinalDistanceY = (FinalVelocityY * ClockTimeintoSeconds) + InitialDistanceY;
		 }
		
		if (_kbhit())
		{
			switch(_getch())
			{
			case KEY_UP :
			Angle+= .5;
			break;
			case KEY_DOWN:
			Angle-= .5;
			break;
			}
		}

		cout << "X Distance : " << FinalDistanceX << " Y Distance : " << FinalDistanceY << " Angle " << Angle <<endl;

		StartTime = EndTime;
		InitialVelocityX = FinalVelocityX;
		InitialVelocityY = FinalVelocityY;
		InitialDistanceX = FinalDistanceX;
		InitialDistanceY = FinalDistanceY;
	}


}