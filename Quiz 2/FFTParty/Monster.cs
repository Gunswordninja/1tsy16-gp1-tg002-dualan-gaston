﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FFTParty
{
    public class Monster
    {
        public string Name { get; private set; }
        public string CharacterToKill { get; private set; }

        public Monster(string name, string target)
        {
            Name = name;
            CharacterToKill = target;
        }
    }
}
