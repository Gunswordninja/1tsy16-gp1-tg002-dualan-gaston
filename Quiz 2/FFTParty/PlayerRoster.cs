﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FFTParty
{
    public class PlayerRoster
    {
        public int Gil { get; set; }
        public List<Character> Characters = new List<Character>();

        public void PrintRoster()
        {
            foreach (Character character in Characters)
            {
                character.PrintMemberDetails();
            }
        }

        /// QUIZ ITEM
        /// Implement the EncounterMonster function, 
        /// this is when a Monster is about attack the party and potentially kill someone.
        /// The Monster's target can be read from monster.CharacterToKill
        /// Search your available party members for a match, if there's a match
        /// Remove that character from your list,
        /// If there isn't a match, just print "Monster wasn't able to kill anything!"
        /// Hint: Linear Search
        public void EncounterMonster(Monster monster)
        {
            List<int> deadChar = new List<int>();
            foreach (Character character in Characters)
            {      
                if (monster.CharacterToKill == character.JobName)
                {
                    Console.WriteLine();
                    Console.WriteLine(character.Name + " has been killed by " + monster.Name + " !");
                    deadChar.Add(Characters.IndexOf(character));
                }
            }
            foreach( int character in deadChar)
            {
                Characters.RemoveAt(character);
            }
        }

        /// QUIZ ITEM
        /// Compute the player's money earned
        /// Loop through each character and get their income
        /// Display the income and add it to the player's Gil
        public void ComputeEarnings()
        {
            int pot = 0;
            foreach (Character character in Characters)
            {
                Console.WriteLine(character.Name + " has earned " + character.Income + " !");
                Console.WriteLine();
                pot += character.Income;
            }
            Console.WriteLine("You earn a total of " + pot + "!");
            Gil += pot;
            Console.WriteLine("Current Gil : " + Gil);
        }
    }
}
