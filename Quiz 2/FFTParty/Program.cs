﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FFTParty
{
    class Program
    {
        static void Main(string[] args)
        {
            Random random = new Random();
            Console.WriteLine("Welcome to Ivalice!");

            PlayerRoster playerRoster = new PlayerRoster();
            playerRoster.Gil = 2000;

            string input = "";
            while (input != "n")
            {
                string recruitInput = "";
                while (recruitInput != "n")
                {
                    Console.Write("Would you like to visit more cities to recruit?(y/n)");
                    recruitInput = Console.ReadLine();
                    if (recruitInput == "n")
                        break;

                    /// QUIZ ITEM
                    /// Print all available cities in the game
                    /// You can get them from GameDatabase.GetCity(int id)
                    /// There are only 6 valid cities, w/ id numbers from 1-6
                    /// Hint: For loop
                    // Print all cities
                    Console.WriteLine();

                    for (int i = 1; i <= 6; i++)
                    {
                        Console.WriteLine(i + "." + GameDatabase.GetCity(i).Name);
                    }

                    // Ask user w/c city he would like to visit
                    Console.Write("Please enter the city you would like to visit:");
                    int cityId = Convert.ToInt32(Console.ReadLine());

                    /// QUIZ ITEM
                    /// Get the city from the database by GameDatabase.GetCity(int id)
                    GameDatabase.GetCity(cityId);
                    Console.WriteLine();
                    /// QUIZ ITEM
                    /// Enter the city by calling city.Recruit()
                    /// You must pass in the player's party
                    GameDatabase.GetCity(cityId).Recruit(playerRoster);

                    Console.WriteLine();
                }
                playerRoster.PrintRoster();
                Console.WriteLine("-----------");

                Console.WriteLine("Your party has travelled Ivalice and encountered monsters!");
                List<Monster> monsters = new List<Monster>();
                int oldPlayerCount = playerRoster.Characters.Count;
                int monsterCount = random.Next(1, 7);
                for (int i = 0; i <= monsterCount; i++)
                {
                    /// QUIZ ITEM
                    /// Randomize a Monster ID from 1-10
                    /// and add it to the monsters list
                    /// You can get an Monster by calling
                    /// GameDatabase.GetMonster(int id)
                    monsters.Add(GameDatabase.GetMonster(random.Next(1, 10)));
                }

                // Fight Monsters
                foreach (Monster monster in monsters)
                {
                    Console.WriteLine("A " + monster.Name + " Appears!");
                    playerRoster.EncounterMonster(monster);
                }

                if (playerRoster.Characters.Count == oldPlayerCount)
                { Console.WriteLine("Nobody in your party died!"); }
                
                Console.WriteLine("Encounter over");
                // Display player's earnings
                playerRoster.ComputeEarnings();

                Console.WriteLine("Would you like to go on another adventure?(y/n)");
                input = Console.ReadLine();
            }
            Console.Read();
        }
    }
}
