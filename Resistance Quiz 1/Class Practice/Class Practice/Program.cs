﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Class_Practice
{
    class Program
    {
        
        static void SetStats(Player Hero, int HP, int MP, int Attack, int Defense, int MagicAttack, int MagicDefense, int Speed)
        {
            Hero.SetHP = HP;
            Hero.SetMP = MP;
            Hero.SetAttack = Attack;
            Hero.SetDefense = Defense;
            Hero.SetMagicAttack = MagicAttack;
            Hero.SetMagicDefense = MagicDefense;
            Hero.SetSpeed = Speed;
            Hero.Str = Attack;
        }

        static void ClassChoice(int Choice, Player Hero)
        {
            bool Selection = true;
          while (Selection == true)
          {
                switch (Choice)
                {
                    case 1: //Warrior
                    SetStats(Hero,150,75,15,15,5,5,10);
                    Selection = false;
                    break;
                    case 2: //Wizard
                    SetStats(Hero, 100, 200, 5, 5, 20, 15, 7);
                    Selection = false;
                    break;
                    case 3: //Rogue
                    SetStats(Hero, 100, 100, 10, 10, 10, 10, 20);
                    Selection = false;
                    break;
                    default: break; 
                }
          }  
        }

        static void Main(string[] args)
        {
            Player Hero = new Player();
            Console.WriteLine("Choose your class.\n 1. Warrior 2. Wizard 3. Rogue");
            ClassChoice(Convert.ToInt32(Console.Read()), Hero);
        }
    }
}
