﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Class_Practice
{
    class Player
    {
        private string Name;
        private string Class;
        private int Level;
        private int HP;
        private int MP;
        private int Attack;
        private int MagicAttack;
        private int Defense;
        private int MagicDefense;
        private int Speed;
        private int EXP;
        private int MaxExp;

        public int Str { private get; set; }

        // Constructor C#
        public Player()
        {
            Name = "insertname";
            Class = "insertclass";
            Level = 1;
            HP = 1;
            MP = 1;
            Attack = 1;
            MagicAttack = 1;
            Defense = 1;
            MagicDefense = 1;
            Speed = 1;
            EXP = 0;
            MaxExp = 100;
        }

        public int SetHP
        {
            get { return SetHP; }
            set { SetHP = value; }
        }

        public int SetMP
        {
            get { return SetMP; }
            set { SetMP = value; }
        }

        public int SetAttack
        {
            get { return SetAttack; }
            set { SetAttack = value; }
        }

        public int SetDefense
        {
            get { return SetDefense; }
            set { SetDefense = value; }
        }

        public int SetMagicAttack
        {
            get { return SetMagicAttack; }
            set { SetMagicAttack = value; }
        }

        public int SetMagicDefense
        {
            get { return SetMagicDefense; }
            set { SetMagicDefense = value; }
        }

        public int SetSpeed
        {
            get { return SetSpeed; }
            set { SetSpeed = value; }
        }
        /*void LevelUp(int NewAttack, int NewDefense, int NewMagicAttack, int NewMagicDefense, int Speed)
        {
            this -> Attack = NewAttack 
        }*/

        public void CombatAttack(Player Hero, int RecieverHP) { }
    }
}
