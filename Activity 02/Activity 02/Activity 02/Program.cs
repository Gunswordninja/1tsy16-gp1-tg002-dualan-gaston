﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Activity_02
{
    class Program
    {
            static void Search(string[] items, string SearchObject)
        {
            bool Found=false;
            for (int x = 0; x < items.Length; x++)
            {   
                int WeaponNumber;
                WeaponNumber = Array.IndexOf(items, SearchObject);
                
                if (SearchObject == items[x])
                {
                    Found = true;
                    Console.WriteLine(WeaponNumber);
                }
            }
            if (!Found)
            { Console.WriteLine("The item is not in the inventory"); }
        }


        static void Main(string[] args)
        {
            string[] weaponitems = { "Sword", "Spear", "Bow","Glaive","Dual Swords","Naginata","Shotgun","Claws"};
            string search;

            Console.WriteLine("Search a weapon");
            search = Console.ReadLine();

            Search(weaponitems,search);
            Console.ReadKey();
        }
    }
}
