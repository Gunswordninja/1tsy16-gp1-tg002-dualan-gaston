﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Activity_3
{
    class Program
    {

        static void Main(string[] args)
        {
            StartUp();
            int PotMoney;
            bool[] Wins = { false, false, false };
            string[] PlayerOptions = { "Rock", "Scissors", "Paper" };
            string[] CpuOptions = { "Rock", "Paper", "Scissors" };
            Console.WriteLine("Insert how much money you would like to insert");
            PotMoney = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Now let us begin the game!");
            for (int i = 0; i < Wins.Length; i++)
            {
                Console.WriteLine("Round " + (i + 1) + "!");
                {   bool Draw = true;
                    while (Draw == true)
                    {
                        bool MisInput = true;
                    Choice:
                        string Choice;
                        Console.WriteLine("Select between Rock, Paper or Scissors.");
                        Choice = Console.ReadLine();
                        for (int x = 0; x < PlayerOptions.Length; x++)
                        {
                            if (Choice == PlayerOptions[x])
                            {
                                Console.WriteLine("You have chosen " + Choice + ".");
                                MisInput = false;
                                break;
                            }
                        }
                        if (MisInput == true)
                        {
                            Console.WriteLine("Invalid option. Please insert Rock, Paper, or Scissors, and also with capital letters");
                            Console.ReadKey();
                            goto Choice;
                        }
                        Random RnD = new Random();
                        int CpuChoice = RnD.Next(0, CpuOptions.Length);
                        Console.WriteLine("Computer has selected " + CpuOptions[CpuChoice] + ".");
                        if (Choice != CpuOptions[CpuChoice])
                        {
                            if ((Choice == "Scissors" && CpuOptions[CpuChoice] == "Paper") || (Choice == "Paper" && CpuOptions[CpuChoice] == "Rock") || (Choice == "Rock" && CpuOptions[CpuChoice] == "Scissors"))
                            {
                                Wins[i] = true;
                                Console.WriteLine("You won this round! Congratulations!");
                            }

                            else
                            {
                                Wins[i] = false;
                                Console.WriteLine("Sorry, You've lost this round.");
                            }
                            Draw = false;
                            Console.WriteLine("Press any button to continue");
                            Console.ReadKey();
                            Console.Clear();
                        }
                        else { Console.WriteLine("Its a draw!, Get ready to replay!"); }
                    }
                }
            }
            WinCheck(Wins[0], Wins[1], Wins[2], PotMoney);
            Console.ReadKey();
        }

        /*        static void Duel(string [] CpuOptions, string [] PlayerOptions, int i, bool [] Wins)
                {
                    bool Draw = true;
                    while (Draw == true)
                    {
                        bool MisInput = true;
                    Choice: 
                        string Choice;
                        Console.WriteLine("Select between Rock, Paper or Scissors.");
                        Choice = Console.ReadLine();
                                    for (int x = 0; x <PlayerOptions.Length; x++)
                                    {
                                        if (Choice == PlayerOptions[x])
                                        {
                                            Console.WriteLine("You have chosen " + Choice + ".");
                                            MisInput = false;
                                            break;
                                        }
                                    }
                                     if (MisInput == true) 
                                     {
                                             Console.WriteLine("Invalid option. Please insert Rock, Paper, or Scissors, and also with capital letters");
                                             Console.ReadKey();
                                             goto  Choice;
                                     }
                        Random RnD = new Random();
                        int CpuChoice = RnD.Next(0, CpuOptions.Length);
                        Console.WriteLine("Computer has selected " + CpuOptions[CpuChoice] + ".");
                                    if (Choice != CpuOptions[CpuChoice])
                                    {
                                        WinCondition(Choice, CpuOptions[CpuChoice], Wins[i]);
                                        Draw = false;
                                        Console.WriteLine("Press any button to continue");
                                        Console.ReadKey();
                                        Console.Clear();
                                    }
                                    else { Console.WriteLine("Its a draw!, Get ready to replay!"); }
                    }
                }
         */
        /*  static void ChoicePrinter(string[] Choices, string Choice)
          {
              bool Selection = false;
   
                     while (Selection == false)
             
              {
                         for (int x = 0; x < Choices.Length; x++)
                      {
                              if (Choice == Choices[x])
                              {
                                  Console.WriteLine("You have chosen " + Choice + ".");
                                  Selection = true;
                                  break;
                              }
                      }

                             if (!Selection)
                             {
                                 Console.WriteLine("Invalid option. Please insert Rock, Paper, or Scissors, and also with capital letters");
                             }
              }     
          }
          */


        /*  static void WinCondition(string PlayerChoice, string ComputerChoice, bool Win)
          {
             if ((PlayerChoice == "Scissors" && ComputerChoice == "Paper") || (PlayerChoice == "Paper" && ComputerChoice == "Rock") || (PlayerChoice == "Rock" && ComputerChoice == "Scissors")) 
             {
                 Win = true;
                 Console.WriteLine("You won this round! Congratulations!");
             }
     
             else 
             {
                 Win = false;
                 Console.WriteLine("Sorry, You've lost this round.");
             }
           }*/


        static void StartUp()
        {
            StartingUp :

            int Choice;
            Console.Write ("Welcome to Rock Paper Scissors Gambling edition!\nPress 1 to continue, and 2 to exit\n");
            Choice = Convert.ToInt32(Console.ReadLine());
            switch (Choice)
            {
                case 1: { Console.Clear(); break; }
                case 2: { Environment.Exit(0); break; }
                default: { goto StartingUp;}
            }
        }

        static void WinCheck(bool Match1, bool Match2, bool Match3, int PotMoney)
        {
            int Winnings;
            int WinCount = Convert.ToInt32(Match1) + Convert.ToInt32(Match2) + Convert.ToInt32(Match3);

            if (WinCount == 0)
            {
                Winnings = 0;
                Console.WriteLine("Sorry, you've earned " + Winnings + "back. You've won nothing! Hooray!");
            }

            if (WinCount > 0)
            {

                    if (WinCount > 2) { WinCount++; }
                int Multiplier = 0;
                    while (Multiplier < WinCount) { Multiplier++; }

                Winnings = PotMoney * (1 + Multiplier);
                Console.WriteLine("Congratulations! You have earned a total of " + Winnings + " in cash! Thank you for playing!");
            }
        }
    }
}