﻿using UnityEngine;
using System.Collections;

public class Colliders : MonoBehaviour {

    // Use this for initialization
    private Rigidbody2D rb;
    public int jumpHeight;

	void Start () {
        
        
	}
	
	// Update is called once per frame
	void Update () {

        Rigidbody2D rb = GetComponent<Rigidbody2D>();
        float xMovement = Input.GetAxis("Horizontal");

        transform.Translate(xMovement/2, 0, 0);

        if (Input.GetKeyDown("Space"))
        {
            this.transform.Translate(xMovement/2 , jumpHeight, 0);
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("TurnOnGravity"))
        {
            this.rb.gravityScale = 1;
        }

        if (other.gameObject.CompareTag("TurnOffGravity"))
        {
            this.rb.gravityScale = 0;
        }
    }
}
