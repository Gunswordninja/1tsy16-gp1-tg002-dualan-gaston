﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Skills_Exersize
{
    class BattleSequence
    {
        public void Battle(Player mainplayer, Monster CurrentMonster)
        {
            Console.Clear();
            Console.WriteLine("You encounter a monster!");
            
           while(true)
            {
                Console.Clear();
                Console.WriteLine("HP : " + mainplayer.HP + " / " + mainplayer.MaxHP);
                Console.WriteLine("MP : " + mainplayer.MP + " / " + mainplayer.MaxMP);
                Console.WriteLine();
                Console.WriteLine(" 1) Attack \n 2) Change Weapon \n 3) Use a consumable item");
                Console.WriteLine();
                Console.WriteLine(" " + CurrentMonster.MonsterName + "\t\tHealth: " + CurrentMonster.HP);

                int Choice = Convert.ToInt32(Console.ReadLine());

                switch (Choice)
                {
                    case 1:
                        int damage = (mainplayer.Attack + mainplayer.playerWeapon.Attack);
                        CurrentMonster.HP -= damage;
                        Console.WriteLine("You attack for " + damage + " damage!");
                        break;
                    case 2:
                        mainplayer.EquipWeapon();
                        break;
                    case 3:
                        mainplayer.UseConsumable(mainplayer);
                        break;
                    default:
                        break;
                }
                mainplayer.ActiveBuffCheck(mainplayer);
                int MonsterDamage = CurrentMonster.Attack;
                Console.WriteLine();
                Console.WriteLine("You have taken " + MonsterDamage + " damage!");
                mainplayer.HP -= MonsterDamage;
                Console.ReadKey();
            }
        }
    }
}
