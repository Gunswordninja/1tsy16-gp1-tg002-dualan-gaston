﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Skills_Exersize
{
    class GameDataBase
    {
        public static ItemBase GetItem(int id)
        {
            switch (id)
            {
                case 1: return new HealingItem("Health Potion", 30, 20, "HP", "A healing potion that fills your health for a small amount");
                case 2: return new HealingItem("Mana Potion\t", 30, 20, "MP", "A mana potion that fills your mana for a small amount");
                case 3: return new HealingItem("Roast Chicken", 50, 40, "HP", "Delicious chicken, its finger lickin good");
                case 4: return new HealingItem("Sponge Cola\t", 50, 40, "MP", "How should I know what sponge tastes like. It works doesn't it?");
                case 5: return new BuffingItem("Vial of Valor", 40, 20, "Attack", 4, "A concoction that raises your attack temporarily (3 turns) ");
                case 6: return new BuffingItem("Drink of Defense", 40, 20, "Defense", 4, "A concoction that raises your defense temporarily (3 turns) ");
                case 7: return new BuffingItem("Burst of MUSCLE", 60, 70, "Attack", 2, "A BURST OF MUSCLE THAT MAKES YOU SUPER STRONG LIKE MAN (1 turn)");
                case 8: return new BuffingItem("Defense of MUSCLE", 60, 70, "Defense", 2, "A BURST OF MUSCLE THAT SHRUGS WEAK PUNY ATTACKS (1 turn)");
                case 9: return new WeaponItem("Broadsword\t", 100, 10, 0, "A long sword capable of cutting the toughest of hides.");
                case 10: return new WeaponItem("Sword and Shield", 100, 5, 5, "A traditional sword and shield, a balance of offense and defense.");
                case 11: return new WeaponItem("Shield Gauntlets", 200, 5, 10, "A pair of defensive gauntlets, only those that wield incredible strength can carry them.");
                case 12: return new WeaponItem("Divine Spear", 200, 20, 0, "A legendary spear that gives the user little to no defense but incredible feats of strength and agility.");
                default: return null;
            }
        }

        public static Monster GetMonster(int id)
        {
            switch (id)
            {
                case 1: return new Monster("Goatman", 50, 3);
                case 2: return new Monster("Cowman", 70, 2);
                case 3: return new Monster("Duckman", 30, 5);
                default: return null;
            }

        }
    }
}
