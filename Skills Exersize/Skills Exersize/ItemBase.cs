﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Skills_Exersize
{
    class ItemBase
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public int Price { get; set; }

        public int Quantity;

        public ItemBase(string name, int price, string description)
        {
            Name = name;
            Price = price;
            Description = description;
        }

        public virtual void shopItemUniqueAtrributes()
        {

        }

        public virtual void Use(Player mainPlayer)
        {
            Console.WriteLine("Item");
        }
    }
}
