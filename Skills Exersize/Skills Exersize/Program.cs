﻿ using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Skills_Exersize
{
    class Program
    {
        static void PathChoice(int choice, Player mainPlayer)
        {
            switch(choice)
            {
                case 1:
                    mainPlayer.EquipWeapon();
                    break;
                case 2:
                    mainPlayer.DisplayInventory();
                    break;
                case 3:
                    Shop Shop = new Shop();
                    Shop.enter(mainPlayer);
                    break;
                case 4:
                    mainPlayer.ViewStats();
                    break;
                case 5:
                    Random random = new Random();
                    BattleSequence battle = new BattleSequence();
                    battle.Battle(mainPlayer, GameDataBase.GetMonster(random.Next(1, 4)));
                    break;
                case 6:
                    Environment.Exit(0);
                    break;
                default:
                    break;

            }
        }
        static void Main(string[] args)
        {
            Console.WriteLine("Welcome to the land of Bogs. What is your name?");

            string name = Console.ReadLine();

            Player mainPlayer = new Player(name, 20, 20, 10, 10, 200);

            while (mainPlayer.HP > 0)
            {


                Console.WriteLine("What would you like to do? \n\n 1) Equip a weapon\n 2) Check your total inventory\n 3) Enter the shop\n 4) Check your Stats\n 5) FIGHT MONSTARS\n 6) Exit");

                int choice = Convert.ToInt32(Console.ReadLine());

                PathChoice(choice, mainPlayer);

                Console.Clear();

            }

        }
    }
}
