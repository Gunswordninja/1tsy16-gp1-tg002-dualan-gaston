﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Skills_Exersize
{
    class BuffingItem : ItemBase
    {
        string StatSelect;
        int StatToIncrease;
        public int Turns;
        public int unbuffedValue;
        public int buffedValue;

        public BuffingItem (string name, int price, int statToIncrease, string statSelect, int turns, string description) : base (name, price, description)
        {
            StatSelect = statSelect;
            StatToIncrease = statToIncrease;
            Turns = turns;
        }

        public override void shopItemUniqueAtrributes()
        {
            Console.WriteLine("Stat Buffed =\t\t"       + StatSelect);
            Console.WriteLine("Stat increased by =\t" + StatToIncrease);
            Console.WriteLine("Turns active =\t\t" + Turns);   
        }

        public override void Use(Player mainPlayer)
        {
            if (StatSelect == "Attack")
            {
                
                buffedValue = mainPlayer.BaseAttack + StatToIncrease;
                unbuffedValue = mainPlayer.BaseAttack;

                if (Turns > 0)
                {
                    mainPlayer.Attack = buffedValue;
                }

                else
                {
                    mainPlayer.Attack = unbuffedValue;
                }
            }

            if (StatSelect == "Defense")
            {
               if (Turns > 0)
                {
                    unbuffedValue = mainPlayer.BaseDefense;
                    buffedValue = mainPlayer.BaseDefense + StatToIncrease;

                    try
                    {
                        mainPlayer.Defense = buffedValue;
                    }
                    finally
                    {
                        mainPlayer.Defense = unbuffedValue;
                    }
                }
      
            }
        }
    }
}
