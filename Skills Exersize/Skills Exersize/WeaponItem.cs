﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Skills_Exersize
{
    class WeaponItem : ItemBase
    {
        public int Attack;
        public int Defense;

        public WeaponItem(string name, int price, int attack, int defense, string description) : base(name, price, description)
        {
            Attack = attack;
            Defense = defense;
        }
        public override void shopItemUniqueAtrributes()
        {
            Console.WriteLine("Weapon Attack =\t\t" + Attack);
            Console.WriteLine("Weapon Defense =\t" + Defense);
        }

        public void Equip (WeaponItem playerWeapon)
        {
            Console.WriteLine(Name + " equipped!");
            playerWeapon.Name = Name;
            playerWeapon.Attack = Attack;
            playerWeapon.Defense = Defense;
        }
    }
}
