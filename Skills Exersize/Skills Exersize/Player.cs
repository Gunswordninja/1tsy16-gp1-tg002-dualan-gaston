﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Skills_Exersize
{
    class Player
    {
        private string Name { get; set; }

        public Player( string name, int hp, int mp, int attack, int defense, int gold)
        {
            Name = name;
            MaxHP = hp;
            HP = MaxHP;
            MaxMP = mp;
            MP = MaxMP;
            BaseAttack = attack;
            BaseDefense = defense;
            Attack = attack;
            Defense = defense;
            Gold = gold;
            playerWeapon = new WeaponItem("Fists", 0, 0, 0, "FISTS");
        }

        public void ViewStats()
        {
            Console.Clear();
            Console.WriteLine();
            Console.WriteLine("Name : \t\t" + Name);
            Console.WriteLine();
            Console.WriteLine("HP : \t\t" + HP);
            Console.WriteLine("MP : \t\t" + MP);
            Console.WriteLine("Base Attack : \t" + Attack);
            Console.WriteLine("Base Defense : \t" + Defense);
            Console.WriteLine("Gold : \t\t" + Gold);
            Console.WriteLine("Current Equipped Weapon : \t" + playerWeapon.Name);
            Console.WriteLine("Weapon Attack Power : \t\t" + playerWeapon.Attack);
            Console.WriteLine("Weapon Defense Power : \t\t" + playerWeapon.Defense);
            Console.ReadKey();
        }

        public void DisplayInventory()
        {
            WeaponItem WepParameter = new WeaponItem("x", 0, 0, 0, "");
            Console.WriteLine("Consumables");
            Console.WriteLine("==================================");

            foreach (ItemBase InventoryItems in Inventory)
            {
                if (InventoryItems.GetType() != WepParameter.GetType())
                {
                   /* if (InventoryItems.Quantity >= 1)
                    {
                        InventoryItems.Quantity++;
                    }
                   */
                    /* if (InventoryItems.Quantity < 1)
                    {
                        
                    }*/
                    InventoryItems.Quantity++;
                    Console.WriteLine(InventoryItems.Name + "\t\t" + InventoryItems.Quantity);
                }
            }

            Console.WriteLine();
            Console.WriteLine("Weapons");
            Console.WriteLine("==================================");

            foreach (ItemBase InventoryItems in Inventory)
            {
                if (InventoryItems.GetType() == WepParameter.GetType())
                {
                    Console.WriteLine(InventoryItems.Name);

                }
            }

            Console.ReadKey();
        }

        public void UseConsumable(Player mainPlayer)
        {
            WeaponItem WepParameter = new WeaponItem("x", 0, 0, 0, "");
            HealingItem HealParameter = new HealingItem("x", 0, 0, "", "");
            BuffingItem BuffParameter = new BuffingItem("x", 0, 0, "", 0, "");

            Console.WriteLine();
            Console.WriteLine("Which item would you like to use?");
            Console.WriteLine();

            foreach (ItemBase InventoryItems in Inventory)
            {
                if (InventoryItems.GetType() != WepParameter.GetType())
                {
                    //InventoryItems.Quantity++;
                    Console.WriteLine((Inventory.IndexOf(InventoryItems) + 1 + ") " + InventoryItems.Name + "\t\t" + InventoryItems.Quantity));
                }
            }

            int Choice = Convert.ToInt32(Console.ReadLine());

            List<int> removeObject = new List<int>();

            foreach (ItemBase InventoryItems in Inventory)
            {
                
                if (InventoryItems.GetType() != WepParameter.GetType())
                {
                    if (Inventory.IndexOf(InventoryItems) + 1 == Choice)
                    {
                        if (InventoryItems.GetType() == HealParameter.GetType())
                        {
                            HealingItem Heal = InventoryItems as HealingItem;
                            Heal.Use(mainPlayer);
                            removeObject.Add((Inventory.IndexOf(InventoryItems)));
                            break;
                        }

                        if (InventoryItems.GetType() == BuffParameter.GetType())
                        {
                            BuffingItem Buff = InventoryItems as BuffingItem;
                            Buff.Use(mainPlayer);
                            ActiveBuffs.Add(InventoryItems as BuffingItem);
                            removeObject.Add((Inventory.IndexOf(InventoryItems)));
                            break;
                        }
                    }
                }
            }

            foreach (int numbers in removeObject)
            {
                Inventory.RemoveAt(numbers);
            }
        }

        public void ActiveBuffCheck(Player mainplayer)
        {
            foreach(BuffingItem activeBuffs in ActiveBuffs)
            {
                activeBuffs.Turns--;
                Console.WriteLine("You're " + activeBuffs.Name + " is active for " + activeBuffs.Turns + " turns.");
                activeBuffs.Use(mainplayer);
            }
        }

        public void EquipWeapon()
        {
            WeaponItem WepParameter = new WeaponItem("x", 0, 0, 0, "");
            Console.WriteLine();
            Console.WriteLine("Choose which weapon you would like to equip");
            Console.WriteLine();

            foreach (ItemBase InventoryItems in Inventory)
            {
                if (InventoryItems.GetType() == WepParameter.GetType())
                {
                    Console.WriteLine((Inventory.IndexOf(InventoryItems) + 1) + ") " + InventoryItems.Name);
                }
            }

            int choice = Convert.ToInt32(Console.ReadLine());

            foreach (ItemBase InventoryItems in Inventory)
            {
                if (InventoryItems.GetType() == WepParameter.GetType())
                {
                    if (choice == (Inventory.IndexOf(InventoryItems) + 1))
                    {
                        WeaponItem InventoryItemsW = InventoryItems as WeaponItem;
                        InventoryItemsW.Equip(playerWeapon);
                    }
                    else break;
                }
            }
        }

        public int MaxHP;
        public int MaxMP;
        public int HP;
        public int MP;
        public int BaseAttack;
        public int BaseDefense;
        public int Attack;
        public int Defense;
        public int Gold;
        public WeaponItem playerWeapon;
        public List<ItemBase> Inventory = new List<ItemBase>();
        public List<BuffingItem> ActiveBuffs = new List<BuffingItem>();
    }
}
