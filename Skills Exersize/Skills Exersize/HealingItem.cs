﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Skills_Exersize
{
    class HealingItem : ItemBase
    {
        public int StatToHeal { get; set; }
        public string SelectedStat { get; set; }

        public HealingItem(string name, int price, int statToHeal, string selectedStat, string itemDescription) : base (name, price, itemDescription)
        {
            StatToHeal = statToHeal;
            SelectedStat = selectedStat;
        }

        public override void Use(Player mainplayer)
        {
            if (SelectedStat == "HP")
            {
                if (mainplayer.HP == mainplayer.MaxHP)
                {
                    Console.WriteLine("You are already at full health!");
                }

                else
                {
                    Console.WriteLine("You restore " + StatToHeal + " " + SelectedStat + "!");

                    mainplayer.HP += StatToHeal;

                    if (mainplayer.HP > mainplayer.MaxHP)
                    {
                        mainplayer.HP = mainplayer.MaxHP;
                    }
                }
            }

            if (SelectedStat == "MP")
            {
                if (mainplayer.MP == mainplayer.MaxHP)
                {
                    Console.WriteLine("You are already at full mana!");
                }

                else
                {
                    Console.WriteLine("You restore " + StatToHeal + " " + SelectedStat + "!");

                    mainplayer.HP += StatToHeal;

                    if (mainplayer.MP > mainplayer.MaxMP)
                    {
                        mainplayer.MP = mainplayer.MaxMP;
                    }
                }
            }
        }

        public override void shopItemUniqueAtrributes()
        {
            Console.WriteLine("Restoration Amount = \t" + StatToHeal);
            Console.WriteLine("Restored Stat = \t" + SelectedStat);
        }
    }
}
