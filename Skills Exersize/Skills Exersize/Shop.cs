﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Skills_Exersize
{
    class Shop
    {
        public List<ItemBase> ShopItems = new List<ItemBase>();

        public void enter(Player MainPlayer)
        {
            for (int i = 1; i <= 12; i++)
            {
                ShopItems.Add(GameDataBase.GetItem(i));
            }

            bool done = false;
            while (!done)
            {
                Console.Clear();
                Console.WriteLine("What would you like to buy? Enter 0 to exit");
                Console.WriteLine();
                Console.WriteLine("Current Gold = " + MainPlayer.Gold);
                Console.WriteLine();
                Console.WriteLine("Item                    Price");
                Console.WriteLine("=============================");
                Console.WriteLine();

                foreach (ItemBase items in ShopItems)
                {
                    Console.WriteLine((ShopItems.IndexOf(items) + 1) + ") " + items.Name + "\t " + items.Price);
                }

                int choice = Convert.ToInt32(Console.ReadLine());

                if (choice == 0)
                {
                    break;
                }

                foreach (ItemBase items in ShopItems)
                {
                    if (choice == ShopItems.IndexOf(items) +1)
                    {
                        Console.Clear();
                        Console.WriteLine();
                        Console.WriteLine("Name :  "             + items.Name);
                        Console.WriteLine("Price : "             + items.Price);
                        Console.WriteLine();
                        Console.WriteLine("======================");
                        Console.WriteLine("Description :  "      + items.Description);
                        Console.WriteLine();
                        items.shopItemUniqueAtrributes();
                        Console.WriteLine();
                        Console.WriteLine("1 For Yes, 2 For No");

                        int buyChoice = Convert.ToInt32(Console.ReadLine());

                        if (buyChoice == 1)
                        {
                            if(MainPlayer.Gold < items.Price)
                            {
                                Console.WriteLine("You can't purchase that you cheapskate!");
                                Console.ReadKey();
                                break;
                            }

                            else
                            {
                                Console.WriteLine("Succesfully purchased " + items.Name + ".");
                                MainPlayer.Gold -= items.Price;
                                MainPlayer.Inventory.Add(items);
                            }
                        }

                        else
                        {
                            break;
                        }

                    }
                }

                
            }
            
        }
    }
}
