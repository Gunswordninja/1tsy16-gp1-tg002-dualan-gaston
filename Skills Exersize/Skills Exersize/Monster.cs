﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Skills_Exersize
{
    class Monster
    {
        public string MonsterName;
        public int HP;
        public int Attack;

        public Monster(string name, int hp, int attack)
        {
            MonsterName = name;
            HP = hp;
            Attack = attack;
        }
    }
}
