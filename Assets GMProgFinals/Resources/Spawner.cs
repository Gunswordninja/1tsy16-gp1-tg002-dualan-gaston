﻿using UnityEngine;
using System.Collections;

public class Spawner : MonoBehaviour {

    float [] positions = { 1.2f, -.2f }; 
    Vector2 ScreenPosition = new Vector2();
    public GameObject Ship1 = new GameObject();
    public GameObject Ship2 = new GameObject();
    public GameObject Ship3 = new GameObject();
    public GameObject Ship4 = new GameObject();
    GameObject[] Choice;
    

	// Use this for initialization
	void Start () {
 
    }
	
	// Update is called once per frame
	void Update () {

        Vector2 camPosition = new Vector2(Random.Range(positions[0], positions[1]), Random.Range(positions[0], positions[1]));
        ScreenPosition = Camera.main.ViewportToWorldPoint(camPosition);

        int length = GameObject.FindGameObjectsWithTag("Enemy").Length;

        if (length < 10)
        {
            int i = Random.Range(0, 3);

            switch (i)
            {
                case 0:
                    Instantiate(Ship1, ScreenPosition, gameObject.transform.rotation);
                    break;
                case 1:
                    Instantiate(Ship2, ScreenPosition, gameObject.transform.rotation);
                    break;
                case 2:
                    Instantiate(Ship3, ScreenPosition, gameObject.transform.rotation);
                    break;
                case 3:
                    Instantiate(Ship4, ScreenPosition, gameObject.transform.rotation);
                    break;
            }
            
        }

	}
}
