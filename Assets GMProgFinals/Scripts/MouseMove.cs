﻿using UnityEngine;
using System.Collections;

public class MouseMove : MonoBehaviour {

    Vector2 position = new Vector2();
 
    public float speed;
    private float startTime;

	// Use this for initialization
	void Start () {
        startTime = Time.time;
	}
	
	// Update is called once per frame
	void Update () {
        position = Input.mousePosition;
        position = Camera.main.ScreenToWorldPoint(position);
        this.transform.position = Vector2.Lerp(this.transform.position, position, speed * Time.deltaTime);
	}

}
