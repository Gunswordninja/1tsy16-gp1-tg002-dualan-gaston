﻿using UnityEngine;
using System.Collections;

public class EnemyAI : Unit {

    public float enemySpeed;
    public float spawnCount;
    public Vector2 target;
    public GameObject enemy;
    public Color hitBlink = new Color(1,0,0,1);
    Color defaultColor = new Color(1,1,1,1);
    public Renderer rend;
    ArrayList allEnemies;


    // Use this for initialization
    void Start() {
        rend = gameObject.GetComponent<Renderer>();
    }

    // Update is called once per frame
    void Update() {


    }

    public void enemyMovement(float speed)
    {
       GameObject Target = GameObject.FindGameObjectWithTag("Player");
       target = Target.transform.position;
       this.gameObject.transform.position = Vector2.MoveTowards(gameObject.transform.position, target, enemySpeed);
    }

    //void OnBecameInvisible()
    //{
    //    if (allEnemies.Count < spawnCount)
    //    {
    //        Instantiate(enemy);
    //        allEnemies.Add(1);
    //    }
    //}

    public void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.gameObject.CompareTag("Bullet"))
        {   
            HP -= 1;   
            if(rend != null)
                rend.material.color = hitBlink;

            StartCoroutine(Timer());
            if (HP <= 0)
            {
                Debug.LogError("DESTROYED");
                Destroy(gameObject);
            }
        }   
    }

    IEnumerator Timer()
    {
        yield return new WaitForSeconds(.5f);
        rend.material.color = defaultColor;
    }

}
