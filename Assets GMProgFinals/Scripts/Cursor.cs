﻿using UnityEngine;
using System.Collections;

public class Cursor : MonoBehaviour {

    // Use this for initialization
    Vector2 position = new Vector2();
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        position = Input.mousePosition;
        this.transform.position = Vector2.Lerp(Camera.main.ScreenToWorldPoint(position), Camera.main.ScreenToWorldPoint(position), 1);
    }
}
