﻿using UnityEngine;
using System.Collections;

public class CameraMove : MonoBehaviour {

    // Use this for initialization

    bool moving;
    Vector3 playerPosition;
    Vector3 offset;
    Vector3 Velocity = Vector3.zero;

    void Start () {

        offset = gameObject.transform.position;
	}
	
	// Update is called once per frame
	void Update ()
    {
       /* if (moving == true)
        {
           
        }*/

        transform.position = Vector3.SmoothDamp(transform.position, (playerPosition + offset), ref Velocity, .5f);
    }

    void OnTriggerStay2D (Collider2D coll)
    {
        if (coll.gameObject.CompareTag("Player"))
        {
            playerPosition = coll.transform.position;
            moving = true;
        }
    }

   /* void OnTriggerStay2D (Collider2D coll)
    {

    }*/
}
