﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class ShipMovement : EnemyAI
{

    // Use this for initialization
    bool moving;
    bool shooting;
    bool isShooting;
    public int firingPattern;
    public float enemyFirerate;
    public float bulletSpeed;
    private float changingAngle;
    private float changingAngle2;
    public GameObject enemyshot;
    public Vector3 target = new Vector2();
    
    void Start()
    {
        moving = true;
        shooting = false;
        isShooting = true;
        changingAngle2 = 180;
    }

    // Update is called once per frame
    void Update()
    {
        GameObject objectToFind = GameObject.FindGameObjectWithTag("Player");
        target = gameObject.transform.position - objectToFind.transform.position;

        changingAngle += 360 * Time.deltaTime;
        changingAngle2 += 360 * Time.deltaTime;

        if (changingAngle == 360)
        { changingAngle = 0; }

        if (changingAngle2 == 360)
        {
            changingAngle2 = 0;
        }

        if (shooting && isShooting)
        {
            StartCoroutine(ShootThings());
        }
    }

    void LateUpdate()
    {
        if (moving)
        { enemyMovement(enemySpeed); }
    }

    void OnTriggerEnter2D(Collider2D coll)
    {
        base.OnTriggerEnter2D(coll);
        //Debug.Log("Collider detected" + coll);
        if (coll.gameObject.CompareTag("EnemyRange"))
        {
            moving = false;
            shooting = true;
        }
    }

    void OnTriggerExit2D(Collider2D coll)
    {
        if (coll.gameObject.name == "Enemy Shooting Radius")
        {
            moving = true;
            shooting = false;
        }
    }

    IEnumerator ShootThings()
    {
        isShooting = false; // DO NOT DELETE THIS
        yield return new WaitForSeconds(enemyFirerate);
        ShootingTests(firingPattern);
        isShooting = true; // DO NOT DELETE THIS
    }

    Vector2 ShootingPatterns(int patternCode, float spreadAngle)
    {
        spreadAngle = spreadAngle * Mathf.Deg2Rad;

        switch (patternCode)
        {
            case 1:
                {
                    return target.normalized - new Vector3(Mathf.Cos(spreadAngle), Mathf.Sin(spreadAngle), 0);
                }

            case 2:
                {
                    return target.normalized + new Vector3(Mathf.Cos(spreadAngle), Mathf.Sin(spreadAngle), 0);
                }

            default:
                {
                    Vector3 result = new Vector3(Mathf.Cos(spreadAngle), Mathf.Sin(spreadAngle), 0);
                    return result;
                }
        }
    }

    void ShootingTests2(float angle)
    {
        GameObject enemyProjectile = Instantiate(enemyshot, gameObject.transform.position, gameObject.transform.rotation) as GameObject;
        enemyProjectile.GetComponent<Rigidbody2D>().velocity = ShootingPatterns(3, angle).normalized * bulletSpeed;
        
    }

    void ShootingTests(int choice)
    {
        switch (choice)
        {
            case 1:

                for (int i = 0; i < 360; i += 15)
                {
                    GameObject enemyProjectile = Instantiate(enemyshot, gameObject.transform.position, gameObject.transform.rotation) as GameObject;
                    enemyProjectile.GetComponent<Rigidbody2D>().velocity = ShootingPatterns(3, i).normalized * bulletSpeed;
                }
                break;
            case 2:
                for (int i=1; i<4; i++)
                {
                    GameObject enemyProjectile = Instantiate(enemyshot, gameObject.transform.position, gameObject.transform.rotation) as GameObject;
                    switch (i)
                    {
                        case 1:
                            enemyProjectile.GetComponent<Rigidbody2D>().velocity = -target.normalized * bulletSpeed;
                            break;
                        case 2:
                            enemyProjectile.GetComponent<Rigidbody2D>().velocity = -ShootingPatterns(1, 30).normalized * bulletSpeed;
                            break;
                        case 3:
                            enemyProjectile.GetComponent<Rigidbody2D>().velocity = -ShootingPatterns(1, 30).normalized * bulletSpeed;
                            break;
                    }
                }
                break;
            case 3:
                // Spiral Spawn
                {
                    GameObject enemyProjectile = Instantiate(enemyshot, gameObject.transform.position, gameObject.transform.rotation) as GameObject;
                    enemyProjectile.GetComponent<Rigidbody2D>().velocity = ShootingPatterns(3, changingAngle).normalized * bulletSpeed;
                }
                break;
            case 4:
                // Direct Fire
                {
                    GameObject enemyProjectile = Instantiate(enemyshot, gameObject.transform.position, gameObject.transform.rotation) as GameObject;
                    enemyProjectile.GetComponent<Rigidbody2D>().velocity = -target.normalized * bulletSpeed;
                }
                break;
        } 
    }

    IEnumerator ShootingPatternTest(int i)
    {
        yield return new WaitForSeconds(.1f);
        GameObject enemyProjectile = Instantiate(enemyshot, gameObject.transform.position, gameObject.transform.rotation) as GameObject;
        enemyProjectile.GetComponent<Rigidbody2D>().velocity = ShootingPatterns(3, i).normalized * bulletSpeed;
    }
}
