﻿using UnityEngine;
using System.Collections;

public class Firing : MonoBehaviour
{

    float lastTime;
    float targetDistance;
    public float projectileSpeed;
    public float fireRate;
    bool isShooting;
    bool inRange;
    public GameObject shot;
    CircleCollider2D circleCollider;
    private Vector2 shootingDirection = new Vector2();
    private Vector3 enemyPosition = new Vector2();

    // Use this for initialization
    void Start()
    {
        circleCollider = GetComponent<CircleCollider2D>();
        isShooting = false;
    }

    // Update is called once per frame
    void Update()
    {
        targetDistance = 0;

        GameObject[] CurretTargets = GameObject.FindGameObjectsWithTag("Enemy");

        for (int i = 0; i < CurretTargets.Length; i++)
        {
            float distance = Vector2.Distance(gameObject.transform.position, CurretTargets[i].transform.position);

            if (i == 1)
            {
                targetDistance = distance;
                enemyPosition = CurretTargets[i].transform.position;
            }

            if (distance < targetDistance)
            {
                targetDistance = distance;
                enemyPosition = CurretTargets[i].transform.position;
            }

        }

        if (Time.time > fireRate + lastTime)
        {
            shootingDirection = (this.transform.position - enemyPosition).normalized;
            lastTime = Time.time;
            isShooting = true;
        }

        if (isShooting == true)
        {
            GameObject projectile = Instantiate(shot, this.transform.position, this.transform.rotation) as GameObject;
            projectile.GetComponent<Rigidbody2D>().velocity = -shootingDirection.normalized * projectileSpeed;
            isShooting = false;
        }
    }
}

//    //For every projectile add it to a list
//    void OnTriggerStay2D (Collider2D coll)
//    {
//       
//    }
//}
