﻿using UnityEngine;
using System.Collections;

public class BulletHoming : MonoBehaviour {

    float distance { get; set; }
    Vector2 TargetPosition = new Vector2();
    ArrayList distanceComparison = new ArrayList();
    public GameObject enemy;

	// Use this for initialization
	void Start ()
    {
        //enemy = enemy.GetComponent<GameObject>();
        distanceComparison.Add(GameObject.FindGameObjectsWithTag("Enemy"));

        foreach (GameObject enemy in distanceComparison)
        {
            float tempDistance = (Vector2.Distance(this.gameObject.transform.position, enemy.transform.position));
            if (distanceComparison.Count == 0)
            {
                distance = tempDistance;
                this.TargetPosition = enemy.transform.position;
            }

            if (tempDistance < distance)
            {
                distance = tempDistance;
                this.TargetPosition = enemy.transform.position;
            }
        } 
    }
	
	// Update is called once per frame
	void Update ()
    {
        this.gameObject.transform.Translate(TargetPosition.normalized * 100 * Time.deltaTime);
    }
}
