﻿using UnityEngine;
using System.Collections;

public class DeleteBullet : MonoBehaviour {

    // Use this for initialization
    public GameObject projectile;
	void Start () {
       
	}
	
	// Update is called once per frame
	void Update () {
        projectile = gameObject.GetComponent<GameObject>();
    }

    void OnBecameInvisible()
    {
        DestroyObject(this.gameObject);
    }

    void OnTriggerEnter2D (Collider2D coll)
    {
        if (coll.gameObject.CompareTag("Enemy"))
        {
            if (gameObject.CompareTag("Bullet"))
            {
                DestroyObject(gameObject);
            }            
        }

        //if (coll.gameObject.CompareTag("Player"))        
        //{
        //    if (this.gameObject.CompareTag("EnemyBullet"))
        //    {
        //        DestroyObject(gameObject);
        //    }
        //}
    }
}
