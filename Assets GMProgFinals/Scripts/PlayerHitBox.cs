﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayerHitBox : Unit {

    public Image Lives;
    public GameObject canvas;
    ArrayList heartlist = new ArrayList();
    Color defaultColor = new Color(1, 0, 0, 1);
    public Color hitBlink = new Color(1, 0, 0, 1);
    public Renderer rend;
    bool InvurnState;
    bool hit;
    public float InvurnSeconds;
    float addX;

    // Use this for initialization
    void Start () {

        hit = false;
        InvurnState = false;
        rend = gameObject.GetComponent<Renderer>();

        for (int i = 0; i < HP; i++)
        {
            Image Life = Instantiate(Lives) as Image;
            //Lives.rectTransform.anchoredPosition = 
            Life.transform.SetParent(canvas.transform, false);
            Life.transform.localPosition = new Vector3(-600 + addX, 350, 0);
            addX += 75;
            heartlist.Add(Life);
        }
       // Debug.Break();
    }
	
	// Update is called once per frame
	void Update () {

       
	}

    void OnTriggerEnter2D(Collider2D coll)
    {   
        if (coll.gameObject.CompareTag("EnemyBullet"))
        {
            if (InvurnState == false)
            {
                HP--;
                foreach (Image Heart in heartlist)
                {
                    if (HP == heartlist.IndexOf(Heart))
                    { Heart.enabled = false; }
                }
                Lives.enabled = false;
                Debug.Log("You got hit!");
                InvurnState = true;
                if (InvurnState == true)
                { StartCoroutine(HitBlink()); }                
            }
        }
    }

    IEnumerator InvurnerableState()
    {
        yield return new WaitForSeconds(InvurnSeconds);
        StartCoroutine(Blinking());
    }

    IEnumerator HitBlink()
    {
        for (int i = 0; i < 4; i++)
        {
            yield return new WaitForSeconds(.25f);
            rend.material.color = Color.red;
            yield return new WaitForSeconds(.25f);
            rend.material.color = Color.white;
            yield return new WaitForSeconds(.25f);
            rend.material.color = Color.red;
            yield return new WaitForSeconds(.25f);
            rend.material.color = Color.white;
        }
        InvurnState = false;
    }

    IEnumerator Blinking()
    {
        rend.material.color = Color.white;
        yield return new WaitForSeconds(1);
        rend.material.color = defaultColor;
    }
}
