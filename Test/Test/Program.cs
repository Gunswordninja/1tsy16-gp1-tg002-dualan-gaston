﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Activity5
{
    public class Monster
    {
        public Monster()
        {
            Name = "Insert";
            Level = 1;
            HealthPoints = 1;
        }
        
        public string Name
        {
            get
            {
                return Name;
            }
            set
            {
                Name = value;
            }
        }
        public int Level
        {
            get
            {
                return Level;
            }
            set
            {
                Level = value;
            }
        }

        public int HealthPoints
        {
            get
            {
                return HealthPoints;
            }
            set
            {

                HealthPoints = value;
            }
        }

        //  public void SetName(string newName)
        //    {
        //        Name = newName;
        //   }

        //   public int SetLevel(int newLevel)
        //  {

        //      return newLevel;
        //  }
        //  public int SetHP(int newHP)
        //  {
        //      return newHP;
        //  }

        //  public int newLevel()
        //  {
        //      return Level;
        //  }
        //  public int newHP()
        //  {
        //     return HP;
        //  }
        //  public string newName()
        //  {
        //      return Name;
        //  }
        // }



        class Program
        {
            static void EnemyMaker(List<Monster> MonsterList)
            {
                Monster Stats = new Monster();

                Console.Write("Enter Name : ");
                Stats.Name = Console.ReadLine();


                ///////////////////////////////////////

                Console.WriteLine("Enter HP : ");
                Stats.HealthPoints = Convert.ToInt32(Console.ReadLine());

                //////////////////////////////////////
                Console.WriteLine("Enter Level : ");
                Stats.Level = Convert.ToInt32(Console.ReadLine());

                MonsterList.Add(Stats);
            }

            static void EnemyPrinter(List<Monster> MonsterList)
            {
                foreach (Monster Stats in MonsterList)
                {
                    Console.WriteLine("Name: " + Stats.Name + "\n Level : " + Stats.Level + "\n HP :  " + Stats.HealthPoints);
                }
            }




            static void Main(string[] args)
            {

                List<Monster> MonsterList = new List<Monster>();
                Console.WriteLine("How Many Monsters Would You Like?");
                int MonsterAmount = Convert.ToInt32(Console.ReadLine());

                if (MonsterAmount < 5)
                {
                    Console.WriteLine("You do not have enough Monsters");
                    Console.ReadKey();
                }
                if (MonsterAmount > 10)
                {
                    Console.WriteLine("You have to much Monsters we aint makin a Monster Mash(TM)");
                    Console.ReadKey();
                }

                else
                {
                    Console.WriteLine("You have " + MonsterAmount + " of Monsters now its time to distribute the points");
                    for (int i = 0; i < MonsterAmount; i++)
                    {

                        EnemyMaker(MonsterList);


                    }

                }

                Console.WriteLine("Would you like to remove any enemies or Would you like to print?");
                Console.WriteLine("1.Remove \n 2.Print");
            PlayerChoice:
                int PlayerChoice = Int32.Parse(Console.ReadLine());
                switch (PlayerChoice)
                {
                    case 1:

                        int EnemyToRemove = Convert.ToInt32(Console.ReadLine());
                        MonsterList.RemoveAt(EnemyToRemove - 1);

                        foreach (Monster Stats in MonsterList)
                        {
                            Console.WriteLine(Stats);
                        }
                        break;
                    case 2:

                        EnemyPrinter(MonsterList);
                        break;
                    default:
                        Console.WriteLine("Input Not Found");
                        goto PlayerChoice;

                }

            }
        }
    }
}