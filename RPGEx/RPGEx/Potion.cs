﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGEx
{
    public class Potion
    {
        public Potion (string name, int amount, string type)
        {
            Name = name;
            Amount = amount;
            Type = type;
        }
           
        public string   Name;
        public string   Type;
        public int      Amount;
        public int      quantity;
        public int      cost;
    }
}
