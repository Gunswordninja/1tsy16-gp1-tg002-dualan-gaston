﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// v1.0.01 added character races and stat multipliers prior to selective class, added Mana points to character class and specific leveling stats prior to class
namespace RPGEx
{
    class Program
    {
        static void Encounter (Monster monster, Player mainPlayer)
        {
            while (true)
            {
                // Display hitpoints
                mainPlayer.DisplayHitPoints();
                monster.DisplayHitPoints();
                Console.WriteLine();

                bool runAway = mainPlayer.Attack(monster);

                if (runAway)
                    break;

                if (monster.isDead)
                {
                    mainPlayer.Victory(monster.ExpReward);
                    mainPlayer.GetMoney(monster.gold);
                    mainPlayer.LevelUp();
                    break;
                }

                monster.Attack(mainPlayer);

                if (mainPlayer.isDead)
                {
                    mainPlayer.GameOver();
                    //done = true;
                    break;
                }
            }

        }
        static void Main(string[] args)
        {
            Map gameMap = new Map();
            Player mainPlayer = new Player();
            mainPlayer.CreateClass();

            // Begin adventure
            bool done = false;
            while (!done)
            {
               
                // Each loop cycle we output the player position
                // and a selection menu
                gameMap.PrintPlayerPos();

                int selection = 1;
                Console.Write("1) Move, 2) Rest, 3) View Stats, 4) Quit:");
                selection = Convert.ToInt32(Console.ReadLine());
                Monster monster = null;

                switch (selection)
                {
                    case 1:
                        gameMap.MovePlayer();
<<<<<<< HEAD
=======
                        
                        //entering shop
                        if (gameMap.PlayerXPos == 2 && gameMap.PlayerYPos == 3)
                        {
                            Store Store = new Store();
                            Store.Enter(mainPlayer);
                        }
>>>>>>> f915b9c917b5f2443bcb2eb44f575a859bc89fa8

                        // Check for a random encounter. This function
                        // returns a null pointer if no monster are
                        // encoutered.
                        monster = gameMap.CheckRandomEncounter();
<<<<<<< HEAD

=======
>>>>>>> f915b9c917b5f2443bcb2eb44f575a859bc89fa8
                        if (monster != null)
                        {
                            Encounter(monster, mainPlayer);                           
                        }
                        break;
                    case 2:
                        int roll = RandomHelper.Random(1, 4);
                        if (roll == 1)
                        {
                            Console.WriteLine("You hear a rustling in the bushes!");
                            monster = gameMap.CheckRandomEncounter();
                            if (monster != null)
                            {
                                Encounter(monster, mainPlayer);
                            }
                            else
                            {
                                Console.WriteLine("It was just a rabbit.");
                            }
                        }
                        mainPlayer.Rest();
                        break;
                    case 3:
                        mainPlayer.ViewStats();
                        break;
                    case 4:
                        done = true;
                        break;
                }
            }
        }
    }
}
