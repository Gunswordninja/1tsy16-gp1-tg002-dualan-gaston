﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGEx
{
    class Armor
    {
        public Armor (string Name, int Armor, int Cost)
        {
            name = Name;
            armor = Armor;
            cost = Cost;
        }
        public string name;
        public int armor;
        public int cost;
    }
}
